from django.urls import path
from . import views

app_name= 'photoalbum'

urlpatterns = [
    path("", views.gallery, name='gallery'),
    path("photo/add/", views.addPhoto, name="add"),
    path("photo/<str:pk>/", views.viewPhoto, name="photo"),

]